# Deck of Cards

Simple client app for https://deckofcardsapi.com/

## Used tools

* Android Architecture Components (ViewModel, LiveData)
* Retrofit
* RxJava
* Dagger 2
* Glide
* GSON