package wkolendo.deckofcards.repos.rest;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import wkolendo.deckofcards.models.DrewCards;
import wkolendo.deckofcards.models.ShuffledCards;

/**
 * @author Wojtek Kolendo
 */
public interface DeckApi {

	/**
	 * Draw specific number of cards from existing deck.
	 *
	 * @param deckId id of the deck from which cards will be drew
	 * @param count number of cards, which will be drew from deck
	 * @return {@link DrewCards}
	 */
	@GET("{deckId}/draw")
	Single<DrewCards> drawCards(@Path("deckId") String deckId, @Query("count") String count);

	/**
	 * Creates a new shuffled deck.
	 *
	 * @param deckCount number of card decks
	 * @return {@link ShuffledCards} with newly created deck
	 */
	@GET("new/shuffle")
	Single<ShuffledCards> shuffleCards(@Query("deck_count") String deckCount);

	/**
	 * Reshuffles the cards in specific deck with deckId.
	 * Don't throw away a deck when all you want to do is shuffle.
	 *
	 * @param deckId id of the whole deck, which will be shuffled
	 * @param deckCount number of card decks
	 * @return {@link ShuffledCards}
	 */
	@GET("{deckId}/shuffle")
	Single<ShuffledCards> shuffleCards(@Path("deckId") String deckId, @Query("deck_count") String deckCount);

}
