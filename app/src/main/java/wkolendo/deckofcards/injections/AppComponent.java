package wkolendo.deckofcards.injections;

import javax.inject.Singleton;

import dagger.Component;
import wkolendo.deckofcards.views.fragments.DeckFragment;
import wkolendo.deckofcards.views.fragments.StartupFragment;

/**
 * @author Wojtek Kolendo
 */
@Component(modules = {AppModule.class, RestModule.class})
@Singleton
public interface AppComponent {

	void inject(DeckFragment deckFragment);

	void inject(StartupFragment startupFragment);
}