package wkolendo.deckofcards.injections;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import wkolendo.deckofcards.DeckOfCardsApplication;
import wkolendo.deckofcards.repos.rest.DeckApi;
import wkolendo.deckofcards.viewmodels.DeckViewModelFactory;

/**
 * @author Wojtek Kolendo
 */

@Module
public class AppModule {

	private DeckOfCardsApplication deckOfCardsApplication;

	public AppModule(DeckOfCardsApplication deckOfCardsApplication) {
		this.deckOfCardsApplication = deckOfCardsApplication;
	}

	@Provides
	@Singleton
	Context provideContext() {
		return deckOfCardsApplication;
	}

	@Provides
	@Singleton
	DeckViewModelFactory provideViewModelFactory(DeckApi deckApi) {
		return new DeckViewModelFactory(deckApi);
	}
}
