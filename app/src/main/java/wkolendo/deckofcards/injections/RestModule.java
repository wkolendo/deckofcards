package wkolendo.deckofcards.injections;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import wkolendo.deckofcards.BuildConfig;
import wkolendo.deckofcards.repos.rest.DeckApi;

/**
 * @author Wojtek Kolendo
 */

@Module
public class RestModule {

	@Provides
	@Singleton
	OkHttpClient provideHttpClient() {
		OkHttpClient.Builder client = new OkHttpClient.Builder();
		client.addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC));
		return client.build();
	}

	@Provides
	@Singleton
	Gson provideGson() {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
		return gsonBuilder.create();
	}

	@Provides
	@Singleton
	@Named("DECK_API")
	Retrofit provideRetrofit(Gson gson, OkHttpClient okHttpClient) {
		return new Retrofit.Builder()
				.baseUrl(BuildConfig.API_URL)
				.addConverterFactory(GsonConverterFactory.create(gson))
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.client(okHttpClient)
				.build();
	}

	@Provides
	@Singleton
	DeckApi provideDeckApi(@Named("DECK_API") Retrofit retrofit) {
		return retrofit.create(DeckApi.class);
	}

}
