package wkolendo.deckofcards.viewmodels;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import software.rsquared.androidlogger.Logger;
import wkolendo.deckofcards.models.Card;
import wkolendo.deckofcards.models.NetworkState;
import wkolendo.deckofcards.models.SuitType;
import wkolendo.deckofcards.repos.rest.DeckApi;

import static wkolendo.deckofcards.models.ValueType.JACK;
import static wkolendo.deckofcards.models.ValueType.KING;
import static wkolendo.deckofcards.models.ValueType.QUEEN;

/**
 * @author Wojtek Kolendo
 */
@SuppressWarnings("FieldCanBeLocal")
public class DeckViewModel extends ViewModel {

	//	number of cards which are drew every time
	private final String DRAW_CARDS_COUNT = "5";

	private DeckApi deckApi;
	private CompositeDisposable compositeDisposable;

	private int numOfDecks;
	private String deckId;
	private int remainingCards;
	private ArrayList<Card> cards;

	private MutableLiveData<NetworkState> shufflingCardsNetworkState = new MutableLiveData<>();
	private MutableLiveData<NetworkState> drewCardsNetworkState = new MutableLiveData<>();

	@Inject
	DeckViewModel(DeckApi deckApi) {
		this.deckApi = deckApi;
		this.compositeDisposable = new CompositeDisposable();
	}

	public void setNumOfDecks(int numOfDecks) {
		this.numOfDecks = numOfDecks;
	}

	public int getRemainingCards() {
		return remainingCards;
	}

	public ArrayList<Card> getCards() {
		return cards;
	}

	public MutableLiveData<NetworkState> getShufflingCardsNetworkState() {
		return shufflingCardsNetworkState;
	}

	public MutableLiveData<NetworkState> getDrewCardsNetworkState() {
		return drewCardsNetworkState;
	}

	public void clearCards() {
		if (cards != null) {
			cards.clear();
		}
	}

	/**
	 * Shuffles new cards into deck
	 */
	public void executeShuffleCards() {
		compositeDisposable.add(deckApi.shuffleCards(String.valueOf(numOfDecks))
				.subscribeOn(Schedulers.io())
				.doOnSubscribe(disposable1 -> shufflingCardsNetworkState.postValue(NetworkState.LOADING))
				.subscribe(
						result -> {
							deckId = result.getDeckId();
							remainingCards = result.getRemainingCards();
							shufflingCardsNetworkState.postValue(NetworkState.SUCCESS);
						}, e -> {
							Logger.error(e);
							shufflingCardsNetworkState.postValue(NetworkState.FAILED(e));
						}));
	}

	/**
	 * Draws 5 (by default) cards from current deck
	 */
	public void executeDrawCards() {
		if (deckId == null) {
			executeShuffleCards();
			return;
		}
		compositeDisposable.add(deckApi.drawCards(deckId, DRAW_CARDS_COUNT)
				.subscribeOn(Schedulers.io())
				.doOnSubscribe(disposable1 -> drewCardsNetworkState.postValue(NetworkState.LOADING))
				.subscribe(
						drewCards -> {
							remainingCards = drewCards.getRemaining();
							cards = drewCards.getCards();
							drewCardsNetworkState.postValue(NetworkState.SUCCESS);
						}, e -> {
							Logger.error(e);
							drewCardsNetworkState.postValue(NetworkState.FAILED(e));
						}));
	}

	@Override
	protected void onCleared() {
		compositeDisposable.dispose();
		super.onCleared();
	}


	public boolean isColorWin(@NonNull List<Card> list) {
		int spades = 0;
		int hearts = 0;
		int diamonds = 0;
		int clubs = 0;

		for (Card card : list) {
			if (card.getSuit() == SuitType.SPADES) spades++;
			if (card.getSuit() == SuitType.HEARTS) hearts++;
			if (card.getSuit() == SuitType.DIAMONDS) diamonds++;
			if (card.getSuit() == SuitType.CLUBS) clubs++;
		}

		return (spades > 2 || hearts > 2 || diamonds > 2 || clubs > 2);
	}

	public boolean isFiguresWin(@NonNull List<Card> list) {
		int figures = 0;
		for (Card card : list) {
			if (card.getValue() == JACK || card.getValue() == QUEEN || card.getValue() == KING) {
				if (figures == 2) {
					return true;
				}
				figures++;
			}
		}
		return false;
	}

	public boolean isStairsWin(@NonNull List<Card> list) {
		for (int i = 0; i < list.size(); i++) {
			if (i >= 2) {
				int firstValue = list.get(i - 1).getValue().ordinal();
				int secondValue = list.get(i - 2).getValue().ordinal();
				int currentValue = list.get(i).getValue().ordinal();

				if (currentValue > firstValue && firstValue > secondValue) {
					return true;
				}
				if (currentValue < firstValue && firstValue < secondValue) {
					return true;
				}
			}
		}
		return false;
	}

	public boolean isTwinsWin(@NonNull List<Card> list) {
		for (Card card : list) {
			int twins = 0;
			for (Card possiblyTwin : list) {
				if (possiblyTwin.getValue() == card.getValue()) {
					twins++;
				}
			}
			if (twins >= 2) {
				return true;
			}
		}
		return false;
	}
}
