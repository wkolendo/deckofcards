package wkolendo.deckofcards.viewmodels;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import wkolendo.deckofcards.repos.rest.DeckApi;

/**
 * @author Wojtek Kolendo
 */
public class DeckViewModelFactory implements ViewModelProvider.Factory {

	private final DeckApi deckApi;

	@Inject
	public DeckViewModelFactory(DeckApi deckApi) {
		this.deckApi = deckApi;
	}

	@SuppressWarnings("unchecked")
	@NonNull
	@Override
	public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
		if (modelClass.isAssignableFrom(DeckViewModel.class)) {
			return (T) new DeckViewModel(deckApi);
		}
		throw new IllegalArgumentException("Unknown ViewModel Class");
	}
}
