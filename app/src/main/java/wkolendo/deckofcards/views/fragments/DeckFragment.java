package wkolendo.deckofcards.views.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import wkolendo.deckofcards.DeckOfCardsApplication;
import wkolendo.deckofcards.R;
import wkolendo.deckofcards.models.Card;
import wkolendo.deckofcards.models.NetworkState;
import wkolendo.deckofcards.viewmodels.DeckViewModel;
import wkolendo.deckofcards.viewmodels.DeckViewModelFactory;
import wkolendo.deckofcards.views.adapters.CardsRecyclerViewAdapter;

/**
 * @author Wojtek Kolendo
 */
@SuppressWarnings("WeakerAccess")
public class DeckFragment extends DeckOfCardsFragment {

	@Inject
	DeckViewModelFactory viewModelFactory;
	private DeckViewModel viewModel;
	private CardsRecyclerViewAdapter adapter;

	@BindView(R.id.draw_button)
	protected View drawButton;

	@BindView(R.id.shuffle_button)
	protected View shuffleButton;

	@BindView(R.id.remaining_cards)
	protected TextView remainingCardsTextView;

	@BindView(R.id.recycler_view)
	protected RecyclerView recyclerView;

	@BindView(R.id.empty_view)
	protected View emptyView;


	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DeckOfCardsApplication.getAppComponent().inject(this);
		viewModel = ViewModelProviders.of(requireActivity(), viewModelFactory).get(DeckViewModel.class);
		viewModel.getDrewCardsNetworkState().observe(this, this::onDrewCardsStateChanged);
		viewModel.getShufflingCardsNetworkState().observe(this, this::onShuffleCardsStateChanged);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_deck, container, false);
		ButterKnife.bind(this, view);
		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		drawButton.setOnClickListener(v -> {
			if (viewModel.getRemainingCards() > 0) {
				viewModel.executeDrawCards();
			} else {
				showSnackMessage(R.string.deck_no_remaining_cards);
			}
		});

		shuffleButton.setOnClickListener(v -> {
			viewModel.clearCards();
			adapter.notifyDataSetChanged();
			showEmptyView(true);
			viewModel.executeShuffleCards();
		});

		remainingCardsTextView.setText(getString(R.string.deck_remaining_cards, viewModel.getRemainingCards()));

		FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getContext());
		layoutManager.setFlexDirection(FlexDirection.COLUMN);
		layoutManager.setJustifyContent(JustifyContent.FLEX_START);
		recyclerView.setLayoutManager(layoutManager);
		adapter = new CardsRecyclerViewAdapter();
		checkList();
	}

	private void setCardsList(@Nullable List<Card> items) {
		adapter.setItems(items);
		recyclerView.setAdapter(adapter);
	}

	private void onDrewCardsStateChanged(NetworkState networkState) {
		switch (networkState.getStatus()) {
			case LOADING: {
				showLoading(true);
				break;
			}
			case SUCCESS: {
				showLoading(false);
				remainingCardsTextView.setText(getString(R.string.deck_remaining_cards, viewModel.getRemainingCards()));
				checkList();
				break;
			}
			case FAILED: {
				showLoading(false);
				showError(networkState);
				break;
			}
		}
	}

	private void onShuffleCardsStateChanged(NetworkState networkState) {
		switch (networkState.getStatus()) {
			case LOADING: {
				showLoading(true);
				break;
			}
			case SUCCESS: {
				showLoading(false);
				remainingCardsTextView.setText(getString(R.string.deck_remaining_cards, viewModel.getRemainingCards()));
				break;
			}
			case FAILED: {
				showLoading(false);
				showError(networkState);
				break;
			}
		}
	}

	private void showEmptyView(boolean show) {
		emptyView.setVisibility(show ? View.VISIBLE : View.GONE);
	}

	private void checkList() {
		List<Card> list = viewModel.getCards();
		if (list == null || list.isEmpty()) {
			showEmptyView(true);
		} else {
			showEmptyView(false);
			setCardsList(list);

			if (viewModel.isColorWin(list)) {
				showDialogMessage(R.string.deck_win_color, null);
			}
			if (viewModel.isFiguresWin(list)) {
				showDialogMessage(R.string.deck_win_figures, null);
			}
			if (viewModel.isStairsWin(list)) {
				showDialogMessage(R.string.deck_win_stairs, null);
			}
			if (viewModel.isTwinsWin(list)) {
				showDialogMessage(R.string.deck_win_twins, null);
			}
		}
	}
}
