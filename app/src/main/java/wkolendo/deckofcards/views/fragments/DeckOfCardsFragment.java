package wkolendo.deckofcards.views.fragments;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import wkolendo.deckofcards.R;
import wkolendo.deckofcards.models.NetworkState;

/**
 * @author Wojtek Kolendo
 */
public abstract class DeckOfCardsFragment extends Fragment {

	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		if (!(context instanceof wkolendo.deckofcards.views.activities.DeckOfCardsActivity)) {
			throw new RuntimeException(context.getClass().getSimpleName() + " must extends " + wkolendo.deckofcards.views.activities.DeckOfCardsActivity.class.getSimpleName());
		}
	}

	protected void showLoading(boolean show) {
		((wkolendo.deckofcards.views.activities.DeckOfCardsActivity) requireActivity()).showLoading(show);
	}

	protected boolean isLoading() {
		return ((wkolendo.deckofcards.views.activities.DeckOfCardsActivity) requireActivity()).isLoading();
	}

	public Snackbar showSnackMessage(@StringRes int msg, @StringRes int actionMsg, View.OnClickListener listener) {
		return ((wkolendo.deckofcards.views.activities.DeckOfCardsActivity) requireActivity()).showSnackMessage(msg, actionMsg, listener);
	}

	protected Snackbar showSnackMessage(@StringRes int msg) {
		return ((wkolendo.deckofcards.views.activities.DeckOfCardsActivity) requireActivity()).showSnackMessage(msg);
	}

	protected Snackbar showSnackMessage(@StringRes int msg, int duration) {
		return ((wkolendo.deckofcards.views.activities.DeckOfCardsActivity) requireActivity()).showSnackMessage(msg, duration);
	}

	protected Snackbar showSnackMessage(String msg) {
		return ((wkolendo.deckofcards.views.activities.DeckOfCardsActivity) requireActivity()).showSnackMessage(msg);
	}

	protected Snackbar showSnackMessage(String msg, int duration) {
		return ((wkolendo.deckofcards.views.activities.DeckOfCardsActivity) requireActivity()).showSnackMessage(msg, duration);
	}

	protected Toast showToastMessage(@StringRes int msg) {
		return ((wkolendo.deckofcards.views.activities.DeckOfCardsActivity) requireActivity()).showToastMessage(msg);
	}

	protected Toast showToastMessage(@StringRes int msg, int duration) {
		return ((wkolendo.deckofcards.views.activities.DeckOfCardsActivity) requireActivity()).showToastMessage(msg, duration);
	}

	protected Toast showToastMessage(String msg) {
		return ((wkolendo.deckofcards.views.activities.DeckOfCardsActivity) requireActivity()).showToastMessage(msg);
	}

	protected Toast showToastMessage(String msg, int duration) {
		return ((wkolendo.deckofcards.views.activities.DeckOfCardsActivity) requireActivity()).showToastMessage(msg, duration);
	}

	protected void showDialogMessage(String msg, @Nullable final Runnable postAction) {
		new AlertDialog.Builder(requireContext(), R.style.Theme_DeckOfCards_Dialog)
				.setCancelable(false)
				.setMessage(msg)
				.setPositiveButton(android.R.string.ok, (dialog, which) -> {
					if (postAction != null) {
						postAction.run();
					}
				})
				.show();
	}

	protected void showDialogMessage(@StringRes int msg, @Nullable final Runnable postAction) {
		new AlertDialog.Builder(requireContext(), R.style.Theme_DeckOfCards_Dialog)
				.setCancelable(false)
				.setMessage(msg)
				.setPositiveButton(android.R.string.ok, (dialog, which) -> {
					if (postAction != null) {
						postAction.run();
					}
				})
				.show();
	}

	protected void showError(@NonNull NetworkState networkState) {
		((wkolendo.deckofcards.views.activities.DeckOfCardsActivity) requireActivity()).showError(networkState);
	}
}
