package wkolendo.deckofcards.views.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import wkolendo.deckofcards.DeckOfCardsApplication;
import wkolendo.deckofcards.R;
import wkolendo.deckofcards.models.NetworkState;
import wkolendo.deckofcards.viewmodels.DeckViewModel;
import wkolendo.deckofcards.viewmodels.DeckViewModelFactory;
import wkolendo.deckofcards.views.listeners.StartupFragmentListener;

/**
 * @author Wojtek Kolendo
 */
@SuppressWarnings("WeakerAccess")
public class StartupFragment extends DeckOfCardsFragment {

	@Inject
	DeckViewModelFactory viewModelFactory;
	private DeckViewModel viewModel;
	private StartupFragmentListener listener;

	@BindView(R.id.ok_button)
	protected View okButton;

	@BindView(R.id.deck_radios)
	protected RadioGroup deckRadioGroup;


	@Override
	public void onAttach(Context context) {
		Activity activity = getActivity();
		if (activity instanceof StartupFragmentListener) {
			listener = (StartupFragmentListener) activity;
		} else {
			if (activity != null) {
				throw new RuntimeException(activity.getClass().getSimpleName() + " must implements " + StartupFragmentListener.class.getSimpleName());
			}
		}
		super.onAttach(context);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DeckOfCardsApplication.getAppComponent().inject(this);
		viewModel = ViewModelProviders.of(requireActivity(), viewModelFactory).get(DeckViewModel.class);
		viewModel.getShufflingCardsNetworkState().observe(this, this::onShuffleCardsStateChanged);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_startup, container, false);
		ButterKnife.bind(this, view);
		okButton.setOnClickListener(v -> {
			if (listener != null) {
				View selectedRadioButton = deckRadioGroup.findViewById(deckRadioGroup.getCheckedRadioButtonId());
				int selectedValue = deckRadioGroup.indexOfChild(selectedRadioButton) + 1;
				viewModel.setNumOfDecks(selectedValue);
				viewModel.executeShuffleCards();
			}
		});
		return view;
	}

	private void onShuffleCardsStateChanged(NetworkState networkState) {
		switch (networkState.getStatus()) {
			case LOADING: {
				showLoading(true);
				break;
			}
			case SUCCESS: {
				showLoading(false);
				listener.openDeckFragment();
				break;
			}
			case FAILED: {
				showLoading(false);
				showError(networkState);
				break;
			}
		}
	}
}
