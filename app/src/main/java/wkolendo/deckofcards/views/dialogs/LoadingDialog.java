package wkolendo.deckofcards.views.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.view.View;

import androidx.annotation.StyleRes;
import wkolendo.deckofcards.R;


/**
 * @author Wojtek Kolendo
 */

public class LoadingDialog extends Dialog {

	public LoadingDialog(Context context, @StyleRes int theme) {
		this(context, theme, false, null);
	}

	public LoadingDialog(Context context, @StyleRes int theme, boolean cancelable, OnCancelListener cancelListener) {
		super(context, theme);
		setCancelable(cancelable);
		setCanceledOnTouchOutside(cancelable);
		setOnCancelListener(cancelListener);
		init();
	}

	private void init() {
		super.setContentView(R.layout.dialog_loading);
		View backgroundView = findViewById(R.id.background);
		backgroundView.setAlpha(0.8f);
	}
}
