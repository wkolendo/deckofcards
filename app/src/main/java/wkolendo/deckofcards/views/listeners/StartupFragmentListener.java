package wkolendo.deckofcards.views.listeners;

/**
 * @author Wojtek Kolendo
 */
public interface StartupFragmentListener {

	void openDeckFragment();

}
