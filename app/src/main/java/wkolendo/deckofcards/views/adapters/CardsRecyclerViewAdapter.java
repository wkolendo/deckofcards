package wkolendo.deckofcards.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import androidx.recyclerview.widget.RecyclerView;
import wkolendo.deckofcards.R;
import wkolendo.deckofcards.models.Card;

/**
 * @author Wojtek Kolendo
 */
public class CardsRecyclerViewAdapter extends RecyclerView.Adapter<CardsRecyclerViewAdapter.CardViewHolder> {

	private List<Card> items;

	public void setItems(List<Card> items) {
		this.items = items;
	}

	@Override
	public int getItemCount() {
		return items == null ? 0 : items.size();
	}


	@Override
	public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		return new CardViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card, parent, false));
	}

	@Override
	public void onBindViewHolder(CardViewHolder holder, int position) {
		holder.item = items.get(position);
		Card card = holder.item;

		holder.indexTextView.setText(String.valueOf(position + 1));

		Glide
				.with(wkolendo.deckofcards.DeckOfCardsApplication.getInstance())
				.load(card.getImageUrl())
				.into(holder.imageView);
	}

	public class CardViewHolder extends RecyclerView.ViewHolder {

		private Card item;
		private ImageView imageView;
		private TextView indexTextView;

		public CardViewHolder(View itemView) {
			super(itemView);
			imageView = itemView.findViewById(R.id.image);
			indexTextView = itemView.findViewById(R.id.position);
		}
	}
}
