package wkolendo.deckofcards.views.activities;

import android.annotation.SuppressLint;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import wkolendo.deckofcards.R;
import wkolendo.deckofcards.models.NetworkState;
import wkolendo.deckofcards.views.dialogs.LoadingDialog;

/**
 * @author Wojtek Kolendo
 */
public abstract class DeckOfCardsActivity extends AppCompatActivity {

	private LoadingDialog loadingDialog;


	private Snackbar showSnackMessage(Snackbar snackbar) {
		snackbar.show();
		return snackbar;
	}

	public Snackbar showSnackMessage(@StringRes int msg, @StringRes int actionMsg, View.OnClickListener listener) {
		Snackbar snackbar = Snackbar.make(getRootView(), msg, Snackbar.LENGTH_LONG);
		snackbar.setAction(actionMsg, listener);
		return showSnackMessage(snackbar);
	}

	public Snackbar showSnackMessage(@StringRes int msg) {
		return showSnackMessage(msg, Snackbar.LENGTH_LONG);
	}

	public Snackbar showSnackMessage(@StringRes int msg, int duration) {
		return showSnackMessage(Snackbar.make(getRootView(), msg, duration));
	}

	public Snackbar showSnackMessage(String msg) {
		return showSnackMessage(msg, Snackbar.LENGTH_LONG);
	}

	public Snackbar showSnackMessage(String msg, int duration) {
		return showSnackMessage(Snackbar.make(getRootView(), msg, duration));
	}

	private Toast showToastMessage(Toast toast) {
		toast.show();
		return toast;
	}

	public Toast showToastMessage(String msg) {
		return showToastMessage(msg, Toast.LENGTH_SHORT);
	}

	@SuppressLint("ShowToast")
	public Toast showToastMessage(String msg, int duration) {
		return showToastMessage(Toast.makeText(this, msg, duration));
	}

	public Toast showToastMessage(@StringRes int msg) {
		return showToastMessage(msg, Toast.LENGTH_SHORT);
	}


	@SuppressLint("ShowToast")
	public Toast showToastMessage(@StringRes int msg, int duration) {
		return showToastMessage(Toast.makeText(this, msg, duration));
	}

	public void showLoading(boolean show) {
		if (show) {
			if (loadingDialog == null) {
				loadingDialog = new LoadingDialog(this, R.style.Theme_DeckOfCards_ProgressDialog);
			}
			if (!loadingDialog.isShowing()) {
				loadingDialog.show();
			}
		} else {
			if (loadingDialog != null && loadingDialog.isShowing()) {
				loadingDialog.dismiss();
			}
		}
	}

	public boolean isLoading() {
		return loadingDialog != null && loadingDialog.isShowing();
	}

	public void showError(@NonNull NetworkState networkState) {
		if (networkState.getThrowable() != null && !TextUtils.isEmpty(networkState.getThrowable().getMessage())) {
			showSnackMessage(networkState.getThrowable().getMessage());
		} else {
			showSnackMessage(R.string.all_connection_error);
		}
	}


	public ViewGroup getRootView() {
		return (ViewGroup) this.findViewById(android.R.id.content);
	}

}
