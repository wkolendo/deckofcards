package wkolendo.deckofcards.views.activities;

import android.os.Bundle;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;
import wkolendo.deckofcards.R;
import wkolendo.deckofcards.viewmodels.DeckViewModel;
import wkolendo.deckofcards.views.fragments.DeckFragment;
import wkolendo.deckofcards.views.fragments.StartupFragment;
import wkolendo.deckofcards.views.listeners.StartupFragmentListener;

/**
 * @author Wojtek Kolendo
 */
public class MainActivity extends DeckOfCardsActivity implements StartupFragmentListener {

	private static final int FRAGMENT_CONTAINER = R.id.fragment_container;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction().add(FRAGMENT_CONTAINER, new StartupFragment()).commit();
		}
	}

	@Override
	public void openDeckFragment() {
		FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
		transaction.setCustomAnimations(
				R.anim.slide_in_right,
				R.anim.slide_out_left,
				R.anim.slide_in_left,
				R.anim.slide_out_right
		);
		transaction.replace(FRAGMENT_CONTAINER, new DeckFragment());
		transaction.commit();
	}
}
