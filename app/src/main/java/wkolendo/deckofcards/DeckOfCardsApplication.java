package wkolendo.deckofcards;

import android.app.Application;

import software.rsquared.androidlogger.Logger;
import wkolendo.deckofcards.injections.AppComponent;
import wkolendo.deckofcards.injections.AppModule;
import wkolendo.deckofcards.injections.DaggerAppComponent;
import wkolendo.deckofcards.injections.RestModule;

/**
 * @author Wojtek Kolendo
 */
public class DeckOfCardsApplication extends Application {

	private static DeckOfCardsApplication application;
	private static AppComponent appComponent;

	public static DeckOfCardsApplication getInstance() {
		if (application == null) {
			application = new DeckOfCardsApplication();
		}
		return application;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		application = this;
		initLoggerConfig();
		initDagger();
	}

	private void initLoggerConfig() {
		Logger.getLoggerConfig().setLevel(BuildConfig.LOGGER_LEVEL);
	}

	private void initDagger() {
		appComponent = DaggerAppComponent.builder()
				.appModule(new AppModule(application))
				.restModule(new RestModule())
				.build();
	}

	public static AppComponent getAppComponent() {
		return appComponent;
	}
}
