package wkolendo.deckofcards.models;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * @author Wojtek Kolendo
 */
public class NetworkState {

	@NonNull
	private Status status;

	@Nullable
	private Throwable throwable;

	public static final NetworkState LOADING = new NetworkState(Status.LOADING);
	public static final NetworkState SUCCESS = new NetworkState(Status.SUCCESS);
	public static final NetworkState FAILED = new NetworkState(Status.FAILED);

	public static NetworkState FAILED(@Nullable Throwable throwable) {
		return new NetworkState(Status.FAILED, throwable);
	}

	public NetworkState(@NonNull Status status) {
		this.status = status;
	}

	public NetworkState(@NonNull Status status, @Nullable Throwable throwable) {
		this.status = status;
		this.throwable = throwable;
	}

	@NonNull
	public Status getStatus() {
		return status;
	}

	@Nullable
	public Throwable getThrowable() {
		return throwable;
	}

	public enum Status {
		LOADING,
		SUCCESS,
		FAILED
	}
}
