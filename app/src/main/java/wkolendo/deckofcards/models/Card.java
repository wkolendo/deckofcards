package wkolendo.deckofcards.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;


/**
 * @author Wojtek Kolendo
 */
public class Card implements Model {

	@SerializedName("image")
	private String imageUrl;

	@SerializedName("value")
	private ValueType value;

	@SerializedName("suit")
	private SuitType suit;

	@SerializedName("code")
	private String code;

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public ValueType getValue() {
		return value;
	}

	public void setValue(ValueType value) {
		this.value = value;
	}

	public SuitType getSuit() {
		return suit;
	}

	public void setSuit(SuitType suit) {
		this.suit = suit;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.imageUrl);
		dest.writeInt(this.value == null ? -1 : this.value.ordinal());
		dest.writeInt(this.suit == null ? -1 : this.suit.ordinal());
		dest.writeString(this.code);
	}

	public Card() {
	}

	protected Card(Parcel in) {
		this.imageUrl = in.readString();
		int tmpValue = in.readInt();
		this.value = tmpValue == -1 ? null : ValueType.values()[tmpValue];
		int tmpSuit = in.readInt();
		this.suit = tmpSuit == -1 ? null : SuitType.values()[tmpSuit];
		this.code = in.readString();
	}

	public static final Creator<Card> CREATOR = new Creator<Card>() {
		@Override
		public Card createFromParcel(Parcel source) {
			return new Card(source);
		}

		@Override
		public Card[] newArray(int size) {
			return new Card[size];
		}
	};
}
