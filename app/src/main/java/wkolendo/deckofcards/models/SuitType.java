package wkolendo.deckofcards.models;

/**
 * @author Wojtek Kolendo
 */
public enum SuitType {

	CLUBS,
	DIAMONDS,
	HEARTS,
	SPADES;

}
