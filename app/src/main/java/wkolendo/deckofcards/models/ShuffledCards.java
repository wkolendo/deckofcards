package wkolendo.deckofcards.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;


/**
 * @author Wojtek Kolendo
 */
public class ShuffledCards implements Model {

	@SerializedName("success")
	private boolean success;

	@SerializedName("deck_id")
	private String deckId;

	@SerializedName("shuffled")
	private boolean shuffled;

	@SerializedName("remaining")
	private int remainingCards;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getDeckId() {
		return deckId;
	}

	public void setDeckId(String deckId) {
		this.deckId = deckId;
	}

	public boolean isShuffled() {
		return shuffled;
	}

	public void setShuffled(boolean shuffled) {
		this.shuffled = shuffled;
	}

	public int getRemainingCards() {
		return remainingCards;
	}

	public void setRemainingCards(int remainingCards) {
		this.remainingCards = remainingCards;
	}


	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeByte(this.success ? (byte) 1 : (byte) 0);
		dest.writeString(this.deckId);
		dest.writeByte(this.shuffled ? (byte) 1 : (byte) 0);
		dest.writeInt(this.remainingCards);
	}

	public ShuffledCards() {
	}

	protected ShuffledCards(Parcel in) {
		this.success = in.readByte() != 0;
		this.deckId = in.readString();
		this.shuffled = in.readByte() != 0;
		this.remainingCards = in.readInt();
	}

	public static final Creator<ShuffledCards> CREATOR = new Creator<ShuffledCards>() {
		@Override
		public ShuffledCards createFromParcel(Parcel source) {
			return new ShuffledCards(source);
		}

		@Override
		public ShuffledCards[] newArray(int size) {
			return new ShuffledCards[size];
		}
	};
}
