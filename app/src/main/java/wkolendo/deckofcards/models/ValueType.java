package wkolendo.deckofcards.models;

import com.google.gson.annotations.SerializedName;

/**
 * @author Wojtek Kolendo
 */
public enum ValueType {

	ACE,
	@SerializedName("2")
	TWO,
	@SerializedName("3")
	THREE,
	@SerializedName("4")
	FOUR,
	@SerializedName("5")
	FIVE,
	@SerializedName("6")
	SIX,
	@SerializedName("7")
	SEVEN,
	@SerializedName("8")
	EIGHT,
	@SerializedName("9")
	NINE,
	@SerializedName("10")
	TEN,
	JACK,
	QUEEN,
	KING;

}
