package wkolendo.deckofcards.models;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * @author Wojtek Kolendo
 */
public class DrewCards implements Model {

	@SerializedName("success")
	private boolean success;

	@SerializedName("deck_id")
	private String deckId;

	@SerializedName("remaining")
	private int remaining;

	@SerializedName("cards")
	private ArrayList<Card> cards;

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getDeckId() {
		return deckId;
	}

	public void setDeckId(String deckId) {
		this.deckId = deckId;
	}

	public int getRemaining() {
		return remaining;
	}

	public void setRemaining(int remaining) {
		this.remaining = remaining;
	}

	public ArrayList<Card> getCards() {
		return cards;
	}

	public void setCards(ArrayList<Card> cards) {
		this.cards = cards;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeByte(this.success ? (byte) 1 : (byte) 0);
		dest.writeString(this.deckId);
		dest.writeInt(this.remaining);
		dest.writeTypedList(this.cards);
	}

	public DrewCards() {
	}

	protected DrewCards(Parcel in) {
		this.success = in.readByte() != 0;
		this.deckId = in.readString();
		this.remaining = in.readInt();
		this.cards = in.createTypedArrayList(Card.CREATOR);
	}

	public static final Creator<DrewCards> CREATOR = new Creator<DrewCards>() {
		@Override
		public DrewCards createFromParcel(Parcel source) {
			return new DrewCards(source);
		}

		@Override
		public DrewCards[] newArray(int size) {
			return new DrewCards[size];
		}
	};
}
